# Digital Disruption Workshops

A series of one-day workshops addressed to all colleagues with a keen interest into digital tools. These events will allow to create connections, discover and try first hand some of the emerging technologies of today.

See here for more information:
https://moodle.arup.com/course/view.php?id=2473